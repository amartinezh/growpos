//    GrowPOS
//    Copyright (c) 2018 GrowPOS
//    http://GrowPOS.co
//
//    This file is part of GrowPOS
//
//    GrowPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   GrowPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPOS.  If not, see <http://www.gnu.org/licenses/>.
package com.openbravo.data.loader;

import com.openbravo.basic.BasicException;

/**
 * Last modification: 08-08-2018 11:21 am
 * @description This is the interface to connect 
 * to all HttpConnection class
 * @author ceul
 */
public interface Http {
    
    /**
     * 
     * @param url
     * @param body
     * @return
     * @throws BasicException 
     */
    public String post(String url, String body) throws BasicException;
}
